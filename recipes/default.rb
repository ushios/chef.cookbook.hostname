#
# Cookbook Name:: hostname
# Recipe:: default
#
# Copyright 2013, UshioShugo<ushio.s@gmail.com>
#
# All rights reserved - Do Not Redistribute
#

#if node[:hostname] != nil then
	template '/etc/hostname' do
		source 'etc/hostname.erb'
		mode 0644
		owner 'root'
		group 'root'
		variables({
			:hostname => node[:hostname]
		})
	end

	execute "hostname set" do
		command "hostname `cat /etc/hostname`"
		user "root"
	end
#end
